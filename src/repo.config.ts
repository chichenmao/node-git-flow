import { resolve } from 'path'
import { rawList } from './../raw'

const getRepoList = (rawList: object[]) => {
  return rawList.map((rawData: any) => {
    const { author = '', pr = '' } = rawData || {}
    const [group, repo, action, actionNumber, branch] = pr.split('/').filter((info: string) => !!info)

    return {
      name: repo,
      author,
      url: `ssh://git@bitbucket.org:august_team/${repo}.git`,
      branch,
    }
  })
}

export const distFolder = resolve(__dirname, '../temp')
export const repoList = getRepoList(rawList)