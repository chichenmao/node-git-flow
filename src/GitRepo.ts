import { mkdirSync, existsSync } from 'fs'
import { resolve } from 'path'
import { cd, exec } from 'shelljs'

import logger from './logger'

const PUBLIC_DEPENDENCY_REGEXP = /^\^?\d+\.\d+\.\d+.*$/
const PUBLIC_DEPENDENCY_KEYWORDS = ['github']

const CURRENT_PRFIX = '*'
const REMOTE_PREFIX = 'remotes/origin/'
export const SHELL_RESULT_OK = 0

interface IBranchObject {
  current: string
  locals: string[]
  remotes: string[]
}

interface IDependencyObject {
  dependencies: { [key: string]: any }
  devDependencies: { [key: string]: any }
}

interface IMergeObject {
  repo: string
  url: string
  status: '' | 'merged' | 'conflict' | 'duplicated'
  branch: string
  mergeFrom: string
  conflictFiles: any[],
  privacyDependencies: Object
}

interface IGitRepoInit {
  repo: string
  url: string
  dist: string
  tag: string | null
}

class GitRepo {
  repo: string
  repoDir: string
  url: string
  dist: string
  tag: null | string
  branches: IBranchObject
  dependency: IDependencyObject
  privateDependencies: Object
  mergeResult: IMergeObject

  constructor({ repo, url, dist = './', tag = '' }: IGitRepoInit) {
    this.repo = repo
    this.repoDir = ''
    this.url = url
    this.dist = resolve(__dirname, dist)
    this.tag = tag

    this.branches = {
      current: '',
      locals: [],
      remotes: []
    }

    this.dependency = {
      dependencies: {},
      devDependencies: {}
    }

    this.privateDependencies = {}
    this.mergeResult = {
      repo,
      url,
      status: '',
      branch: '',
      mergeFrom: '',
      conflictFiles: [],
      privacyDependencies: {}
    }
  }

  public checkRepoExist() {
    const repoPath = resolve(this.dist, `${this.repo}${this.tag === '' ? '' : `-${this.tag}`}`)
    return existsSync(repoPath)
  }

  public initRepo() {
    logger.logRepo(`Step: Init repo ${this.repo}`)

    if (!existsSync(this.dist)) {
      mkdirSync(this.dist)
    }

    cd(this.dist)
    if (this.checkRepoExist()) {
      cd(this.repo)
    }

    try {
      const { code } = exec(`git clone ${this.url}`)
      if (code === SHELL_RESULT_OK) {
        const { stdout } = exec(`ls -lt ${this.dist}`)
        const [first, latest, ...rest] = stdout.split(/\r|\n/)
        const [fileName, ...others] = latest.split(/\s+/).reverse()
        this.repoDir = `${this.dist}/${fileName}${this.tag === '' ? '' : `-${this.tag}`}`
        if (this.tag !== '') {
          exec(`mv ${this.dist}/${fileName} ${this.repoDir}`)
        }
      }
    } catch (error) {
      logger.logError(`Repo ${this.repo} clone with error: ${error}`)
    }
  }

  public gitBranches() {
    logger.logRepo(`Step: Get branches of repo ${this.repo}`)

    cd(this.repoDir)
    const { stdout, code } = exec('git branch -a')

    if (code !== SHELL_RESULT_OK) {
      logger.logError(`Repo ${this.repo} get branches error: ${stdout}`)
    }

    const branchList = stdout.split(/\r|\n/)

    branchList.forEach(branch => {
      const _branch = branch.trim()
      const currentIndex = _branch.indexOf(CURRENT_PRFIX)
      const remoteIndex = _branch.indexOf(REMOTE_PREFIX)

      if (currentIndex === 0) {
        this.branches.current = _branch.slice(CURRENT_PRFIX.length).trim()
      } else if (remoteIndex === 0) {
        this.branches.remotes.push(_branch.slice(REMOTE_PREFIX.length).trim())
      } else {
        this.branches.locals.push(_branch.trim())
      }
    })
  }

  public switchBranch(target: string) {
    cd(this.repoDir)

    logger.logRepo(`Step: Switch branches to ${target} of repo ${this.repo}`)
    if (this.branches.remotes.includes(target)) {
      const { stdout, code } = exec(`git checkout ${target}`)
      if (code !== SHELL_RESULT_OK) {
        logger.logError(`No exist branch ${target}: ${stdout}`)
      }
    } else if (target === this.tag) {
      exec(`git checkout tags/${target} -b ${target}`)
    }

    this.branches.current = target
    logger.logRepo(`Current branch ${this.branches.current}`)
  }

  public pullCode(target: string = 'master') {
    logger.logRepo(`Pull ${this.repo} with branch ${target}`)
    const { stdout, code } = exec(`git pull origin ${target}`)

    if (code === SHELL_RESULT_OK) {
      logger.logConflict(`Pull ${this.repo}: ${this.branches.current} <--- ${target} info: ${stdout}`)
      this.mergeResult = {
        ...this.mergeResult,
        status: 'merged',
        conflictFiles: []
      }
    } else {
      logger.logConflict(`Pull ${this.repo}: ${this.branches.current} <--- ${target} error: ${stdout}`)
      const { stdout: _stdout } = exec('git diff --name-only --diff-filter=U')
      this.mergeResult = {
        ...this.mergeResult,
        status: 'conflict',
        conflictFiles: [..._stdout.split(/\r|\n/).filter(file => !!file)]
      }
    }

    this.mergeResult = {
      ...this.mergeResult,
      branch: this.branches.current,
      mergeFrom: target,
      privacyDependencies: this.privateDependencies
    }
    return Number(code)
  }

  public getDependencies() {
    cd(this.repoDir)

    const packages = require(`${this.repoDir}/package.json`)
    const { dependencies = {}, devDependencies = {} } = packages

    this.dependency = {
      ...dependencies,
      ...devDependencies
    }

    logger.logRepo(`Repo ${this.repo} dependency loaded`)
    return this
  }

  public getPrivacyDependencies() {
    const dependencyList = Object.keys(this.dependency)
    if (dependencyList.length === 0) {
      logger.logRepo(`Repo ${this.repo} has no privacy dependency`)
    }

    Object.keys(this.dependency).forEach(depName => {
      const version = (<any>this.dependency)[depName].trim()
      if (
        !PUBLIC_DEPENDENCY_REGEXP.test(version)
        && !PUBLIC_DEPENDENCY_KEYWORDS.some(keyword => version.indexOf(keyword) > -1)
      ) {
        this.privateDependencies = {
          ...this.privateDependencies,
          [depName]: version
        }
      }
    })

    logger.logRepo(`Repo ${this.repo} privacy dependency loaded`)
    logger.logRepo(JSON.stringify(this.privateDependencies, null, 2))
  }
}

export default GitRepo