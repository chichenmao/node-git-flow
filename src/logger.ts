import { resolve } from 'path'
import * as Log4js from 'log4js'

const config = {
  appenders: {
    repo: {
      type: 'dateFile',
      filename: resolve(__dirname, '../log/repo'),
      pattern: '-yyyy-MM-dd.log',
      alwaysIncludePattern: true
    },
    error: {
      type: 'dateFile',
      filename: resolve(__dirname, '../log/error'),
      pattern: '-yyyy-MM-dd.log',
      alwaysIncludePattern: true
    },
    conflict: {
      type: 'dateFile',
      filename: resolve(__dirname, '../log/conflict'),
      pattern: '-yyyy-MM-dd.log',
      alwaysIncludePattern: true
    },
    sysError: {
      type: 'dateFile',
      filename: resolve(__dirname, '../log/sysError'),
      pattern: '-yyyy-MM-dd.log',
      alwaysIncludePattern: true
    },
    stdout: {
      type: 'stdout'
    }
  },
  categories: {
    default: {
      appenders: ['repo', 'error', 'conflict', 'stdout'],
      level: 'INFO'
    },
    repo: {
      appenders: ['repo', 'stdout'],
      level: 'INFO'
    },
    error: {
      appenders: ['error', 'stdout'],
      level: 'WARN'
    },
    conflict: {
      appenders: ['conflict', 'stdout'],
      level: 'INFO'
    },
    sysError: {
      appenders: ['sysError', 'stdout'],
      level: 'ERROR'
    }
  },
}

const logger = Log4js.configure(config as Log4js.Configuration)

const repoLogger = logger.getLogger('repo')
const errorLogger = logger.getLogger('error')
const conflictLogger = logger.getLogger('conflict')
const sysErrorLogger = logger.getLogger('sysError')

class Logger {
  public logRepo(message: string) {
    repoLogger.info(message)
  }

  public logError(message: string) {
    errorLogger.error(message)
  }

  public logConflict(message: string) {
    conflictLogger.info(message)
  }

  public logSysError(message: string) {
    const messagePrefix = '****** SYSTEM ERROR START******\n'
    const messageAppend = '****** SYSTEM ERROR END  ******\n'
    const sysErrorMessage = `${messagePrefix}${message}\n${messageAppend}`
    sysErrorLogger.error(sysErrorMessage)
  }
}

export default new Logger()