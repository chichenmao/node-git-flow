# Node-git-flow

This script can check the dependencies of the repo. If the dependencies in the repo has conflict to it's `master` branch, will log in the `conflict.log`.

### Typescript Environment
The script is wrote by typescript, the _global_ Typescript environment is needed.

```shell
npm install -g typescript
```

```shell
npm install -g ts-typescript
```

### Configure
`src/config.js` is to set the repo and the branch.

**example**
```javascript
export const repoList = [
  {
    name: 'august-rest-api',
    url: 'ssh://git@bitbucket-mirror-us.aaecosys.com:7999/bitbucket/ser/august-rest-api.git',
    branch: 'SER-4168-ISS-remove-hard-code-offline-key'
  },
  {
    name: 'august-remote-bridge',
    url: 'ssh://git@bitbucket-mirror-us.aaecosys.com:7999/bitbucket/ser/august-remote-bridge.git',
    branch: 'CHINA-77-add-credential-operation'
  }
]
```

### Run the script
When you finish the `config`, you can run the script by:

```shell
npm run dev
```

After the script finished, you can check the log in `/log` folder.