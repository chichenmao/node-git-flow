import { resolve } from 'path'
import { writeFile } from 'fs'
import { rm, mkdir } from 'shelljs'
import { repoList, distFolder } from './src/repo.config'
import GitRepo, { SHELL_RESULT_OK } from './src/GitRepo'
import logger from './src/logger'

const PRIVATE_PREFIX = 'git+'

rm('-rf', distFolder)

const checkRepoDependencies = ({
  name,
  url,
  branch,
  dist = resolve(__dirname, './temp'),
  pullBranch = 'master',
  tag = ''
}: any) => {
  const repo = new GitRepo({ repo: name, url, dist, tag })

  try {
    if (!repo.checkRepoExist()) {
      repo.initRepo()
      repo.gitBranches()
      repo.switchBranch(branch)
      repo.getDependencies()
      repo.getPrivacyDependencies()

      const pullCode = repo.pullCode(pullBranch)
      if (pullCode === SHELL_RESULT_OK) {
        const privateDependencies = repo.privateDependencies

        const privateDependenciesList = Object.keys(privateDependencies)
        if (privateDependenciesList.length > 0) {
          privateDependenciesList.forEach(depName => {
            const [_url, tag] = (<any>privateDependencies)[depName].slice(PRIVATE_PREFIX.length).split('#')
            const { repo: _repo, ...rest } = checkRepoDependencies({ name: depName, url: _url, branch: tag, tag, dist })

            const { privacyDependencies = {} } = repo.mergeResult
            repo.mergeResult.privacyDependencies = {
              ...privacyDependencies,
              [_repo]: rest
            }
          })
        }
      }
    } else {
      repo.mergeResult = {
        ...repo.mergeResult,
        status: 'duplicated',
        branch,
        mergeFrom: pullBranch
      }
    }
  } catch (err) {
    logger.logSysError(err)
  }

  return repo.mergeResult
}

const checkResult = repoList.map(repo => {
  const { name, url, branch, author } = repo
  const repoTempPath = resolve(distFolder, name)

  mkdir(distFolder)
  mkdir(repoTempPath)

  const analyzeResult = {
    repo: name,
    author,
    url,
    checkBranch: branch,
    privacyDependencies: {}
  }

  const result = checkRepoDependencies({ name, url, branch, dist: repoTempPath, analyzeResult })

  return {
    author,
    ...result
  }
})

writeFile(resolve(__dirname, 'analyze.json'), JSON.stringify(checkResult, null, 2), 'utf8', (err) => {
  if (err) {
    logger.logSysError(JSON.stringify(err, null, 2))
  }

  logger.logRepo('Analyze has down see the result on web page')
})
